#!/bin/bash --login

function exclude_large_files () {
  echo '创建文件夹'
  mkdir Space
  cd Space
  
  echo '初始化Git仓库'
  git init
  if [[ ! $? -eq 0 ]];then echo 'Git仓库初始化失败';exit 1;fi
  
  echo '添加远程仓库地址'
  git remote add origin https://gitlab.com/ff4c00/space.git
  if [[ ! $? -eq 0 ]];then echo '远程仓库地址添加失败';exit 1;fi
  
  echo '开启稀疏检出'
  git config core.sparsecheckout true
  if [[ ! $? -eq 0 ]];then echo '开启稀疏检出失败';exit 1;fi
  
  echo '配置检出文件'
tee .git/info/sparse-checkout <<-'EOF'
/*
!*.wav
!*.flac
!*.ape
!*.wavpack
!*.mp3
!*.aac
!*.vorbis
!*.opus
EOF
  if [[ ! $? -eq 0 ]];then echo '检出文件配置失败';exit 1;fi

  echo '开始拉取远程仓库'
  git fetch
  git checkout master
}

while getopts ":m:" optname
do
  case "$optname" in
    "m")
      $OPTARG
      ;;
    *)
      echo "未知参数项: $OPTIND"
      ;;
  esac
done

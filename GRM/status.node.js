// npm install cli-table3
let Table = require('cli-table3');

// npm install colors
const colors = require('colors');

let mangerInfo = require('./core.js');
const execSync = require('child_process').execSync;

let table = new Table({
  head: ['序号', '本地路径', '当前状态'],
  style: {
    head: [],
    // border: []
  }
});

async function checkRepositoryStatus() {
  let index = -1
  for await (let [key, value] of Object.entries(mangerInfo)) {
    let = key_ = key.split('/')
    let = dirName = key_[key_.length - 1]
    let info = {}
    let changed = '有改动'.red
    let flag = true

    const output = execSync('cd '+value.本地路径+'; git status', { encoding: 'utf-8' })
    if (output.split(/\r\n|\r|\n/).length <= 3) changed = '无改动'
    info[('00' + (index += 1)).slice(-2)] = [value.本地路径, changed]
    table.push(info)
  }
  console.log(table.toString())
}

checkRepositoryStatus();
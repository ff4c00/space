const cmd = require("./cmd.node.js")

let argvs = process.argv.slice(2)
let main_path = process.env.PWD

try {
  repository = JSON.parse(argvs[0])
} catch (e) {
  console.log('参数解析异常: ' + e.toString())
  return ''
}

let target_gitignore = repository.本地路径 + '/.gitignore'
let copy_gitignore = 'cp ' + main_path + '/gitignore ' + target_gitignore
cmd({ 命令: copy_gitignore })

if (repository.Git忽略目录.length > 0){
  ['',repository.Git忽略目录].flat().forEach(dir_name => {
    let add_dir = 'echo "'+dir_name+'" >> '+target_gitignore
    cmd({ 命令: add_dir })
  })
}


const cmd = require("./cmd.node.js")

let argvs = process.argv.slice(2)
let main_path = process.env.PWD

try {
  repository_info = JSON.parse(argvs[0])
} catch (e) {
  console.log('参数解析异常: ' + e.toString())
  return ''
}

for (let [repository_path, repository] of Object.entries(repository_info)) {
  let mkdir = 'mkdir -p ' + repository_path
  cmd({ 命令: mkdir })
}

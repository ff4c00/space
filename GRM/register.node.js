const fs = require('fs')

class Register {
  constructor(reality, ideal) {
    for (let[key, value] of Object.entries(reality)){
      if (["仅包含子仓库"].includes(key)) continue;
      ideal[key] = value
    }

    if (Array.isArray(reality.仅包含子仓库)){
      for (let[key, value] of Object.entries((ideal.子仓库 || {}))){
        if (reality.仅包含子仓库.includes(key)) continue;
        delete ideal.子仓库[key]
      }
    }else if (reality.仅包含子仓库){
      let reality_sub_rep_keys = Object.keys(reality.仅包含子仓库)

      for (let[key, value] of Object.entries((ideal.子仓库 || {}))){
        if (!reality_sub_rep_keys.includes(key)){
          delete ideal.子仓库[key]
          continue
        }
        ideal.子仓库[key] = new Register(reality.仅包含子仓库[key], value);
      }
    }

    for (let[key, value] of Object.entries(ideal)){
      this[key] = value
    }

  }
}

function main(){
  let ideal = JSON.parse(fs.readFileSync('./register.json', 'utf8'))

  let reality
  try {
    reality = JSON.parse(fs.readFileSync('./register.ignore.json', 'utf8'))
  } catch (error) {
    console.error('register.ignore.json文件不存在, 最低要求: ')
    console.log(JSON.stringify({"register.json中主仓库键名":{"本地路径":"项目本地绝对路径"}}, null, 2))
    reality = {}
  }
  
  let result = {}

  for (let[key, value] of Object.entries(reality)){
    let ideal_value = (ideal[key] || {})
    result[key] = new Register(value, ideal_value);
  }

  return result
}

// console.log(JSON.stringify(main(), null, 2))

module.exports = main();

> GRM全称为: Git Repository Manager,用于多git仓库统一维护管理.<br>
起因来自于Gitlab单个账户存储空间限额10G,自建Nas无论是硬件成本还是管理成本都过于高昂繁杂,<br>
而目前个人所有需求只需静态访问即可.<br>
GRM致力于将不同Git仓库整合在同一根目录下,通过为各子目录配置git信息完成灵活配置.

# 说明

## register.json

> 该文件用于注册全部仓库信息

## register.ignore.json

> 该文件用于在register.json基础上管理本地配置.<br>
如本地未配置该文件将默认读取register.json.

例如:

```json
// register.ignore.json
{
  "Space": {
    "本地路径": "/home/ff4c00/SpaceX",
    "仅包含子仓库": {
      "知识体系": {
        "仅包含子仓库": [
          "应用科学/计算机科学/理论计算机科学/编程语言理论/Javascript/word-code"
        ]
      }
    }
  }
}
```

最终本地应用的配置文件为:

```json
{
  "Space": {
    "说明": "根目录,用于存放配置及索引等全局相关信息.",
    "Git用户名": "ff4c00",
    "Git仓库名": "space",
    "主仓库": true,
    "子仓库": {
      "知识体系": {
        "说明": "用于存放各应用层面相关笔记及知识总结等.",
        "Git用户名": "f97d1c",
        "Git仓库名": "knowledge",
        "子仓库": {
          "应用科学/计算机科学/理论计算机科学/编程语言理论/Javascript/word-code": {
            "说明": "将特定词语作为代码进行执行.",
            "Git用户名": "f97d1c",
            "Git仓库名": "word-code"
          }
        }
      }
    },
    "本地路径": "/home/ff4c00/SpaceX"
  }
}
```
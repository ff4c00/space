#!/bin/bash --login
main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

repository_paths=($(jq -r 'keys_unsorted[]' <<<$repositorys_info))

for repository_path in "${repository_paths[@]}"; do
  repository_info=$(jq --arg key $repository_path '."\($key)"' <<<$repositorys_info)
  cd $repository_path
  echo "开始更新: $repository_path"
  git pull origin $(jq -r '."Git分支"' <<<$repository_info)
done

# bash pull.sh
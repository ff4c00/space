#!/bin/bash --login

# 根据各仓库文件最后一次commit hash更新项目

main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

repository_paths=($(jq -r 'keys_unsorted[]' <<<$repositorys_info))
index_file="$main_path/GIT仓库文件索引.json"

function pull_by_commit_index(){
  remote_key="$(git remote get-url --push $(git remote) | sed -E 's/git@(.*).git/\1/' | sed -E 's/https:\/\/(.*).git/\1/' | sed 's/com\//com:/'):$(git branch | sed -E 's/.*\s(.*)/\1/')"

  # 尚未克隆过任何分支的仓库默认master分支
  remote_key=$(sed -E 's/^(.*):$/\1:master/' <<<$remote_key)
  IFS=$'\n' commit_ids=($(jq -r --arg key $remote_key ' .["\($key)"] | keys_unsorted[]' $index_file))

  if [[ ${#commit_ids[@]} -eq 0 ]];then 
    echo "[结果为空] $remote_key"
    return 127; 
  fi

  last_commit_id=$(git log --pretty=format:"%H" 2>/dev/null | head -n 1)

  if [ "$last_commit_id" ];then
    for index in "${!commit_ids[@]}";do
      if [ "${commit_ids[$index]}" == "$last_commit_id" ];then
        unset commit_ids[$index]
        break
      else 
        unset commit_ids[$index]
      fi
    done
  fi

  for index in "${!commit_ids[@]}";do
    commit_id=${commit_ids[$index]}
    echo "[$index/${#commit_ids[@]}][$(date +%Y/%m/%dT%H:%M:%S)][$(git remote get-url --push $(git remote))]: 开始下拉$commit_id"
    count=0
    while [ $count -lt 55 ];do
      git pull $(git remote) $commit_id
      if [[ $? -eq 0 ]]; then
        unset commit_ids[$index]
        count=$((count+=100))
      else
        echo "[$count]拉取失败,继续尝试拉取."
        count=$((count+=1))
      fi
    done
  done
}

for repository_path in "${repository_paths[@]}"; do
  repository_info=$(jq --arg key $repository_path '."\($key)"' <<<$repositorys_info)
  if [ ! -d "$repository_path" ]; then
    echo "尚不存在该文件夹: $repository_path"
    continue;
  fi

  cd "$repository_path"
  pull_by_commit_index
done
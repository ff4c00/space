#!/bin/bash --login
main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

repository_paths=($(jq -r 'keys_unsorted[]' <<<$repositorys_info))

for repository_path in "${repository_paths[@]}"; do
  repository_info=$(jq --arg key $repository_path '."\($key)"' <<<$repositorys_info)
  if [ ! -d "$repository_path" ]; then
    echo "尚不存在该文件夹: $repository_path"
    continue;
  else
    cd $repository_path
    if [ "$(git diff .gitignore)" ];then
      echo "$repository_path 忽略文件存在更改."
      git add .gitignore
      git commit -m '忽略文件更改'
      git push origin master
    fi
  fi

done

# bash commit_gitignore.sh
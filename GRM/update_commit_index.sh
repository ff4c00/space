#!/bin/bash --login

# 将各仓库现有文件的最后一次commit hash汇总收集.

main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

repository_paths=($(jq -r 'keys_unsorted[]' <<<$repositorys_info))
index_file="$main_path/GIT仓库文件索引.json"

function update_file_commit_index(){
  file_index=$(mktemp)
  echo '{}' > $file_index

  find * -type f -print0 | while IFS= read -r -d $'\0' file; do 
    commit_id=$(git log -n 1 --pretty=format:%H -- "$file")
    if [ ! "$commit_id" ];then continue;fi
    result=$(jq --arg key $commit_id --arg value "$file" ' .["\($key)"] = $value' $file_index)
    jq -e '.' <<<"$result" 1>/dev/null
    if [[ $? -eq 0 ]]; then
      echo "[写入] $file"
      echo "$result" > $file_index
    else
      echo -e "[异常]: 跳过写入:\ncommit_id: $commit_id\nfile:$file"
    fi
  done

  IFS=$'\n' array=($(git log --pretty=format:"%H"))

  remote_key="$(git remote get-url --push $(git remote) | sed -E 's/git@(.*).git/\1/' | sed -E 's/https:\/\/(.*).git/\1/' | sed 's/com\//com:/'):$(git branch | sed -E 's/.*\s(.*)/\1/')"

  echo "$(jq --arg key $remote_key ' .["\($key)"] = {}' $index_file)" > $index_file

  for (( index=${#array[@]}-1 ; index>=0 ; index-- )) ; do
    commit_id=${array[index]}
    value=$(jq -r --arg key $commit_id ' .["\($key)"]' $file_index)
    if [ "$value" == 'null' ];then
      echo "[跳过] 已过期ID: $commit_id"
      continue
    fi
    
    commit_content="$(git log -1 --format="%s" $commit_id)"

    echo "$(jq --arg key $remote_key --arg commit_id $commit_id  --arg value $commit_content ' .["\($key)"]["\($commit_id)"] = $value' $index_file)" > $index_file
  done

  rm -f $file_index
}

for repository_path in "${repository_paths[@]}"; do
  repository_info=$(jq --arg key $repository_path '."\($key)"' <<<$repositorys_info)
  if [ ! -d "$repository_path" ]; then
    echo "尚不存在该文件夹: $repository_path"
    continue;
  fi

  cd "$repository_path"
  update_file_commit_index
done

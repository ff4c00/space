#!/bin/bash --login
main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

repository_paths=($(jq -r 'keys_unsorted[]' <<<$repositorys_info))

for repository_path in "${repository_paths[@]}"; do
  repository_info=$(jq --arg key $repository_path '."\($key)"' <<<$repositorys_info)
  result=$(node "$main_path/editGitignore.node.js" "$repository_info" 2>&1)
  echo "$result"
done

# bash gitignore.sh
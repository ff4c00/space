#!/bin/bash --login
main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

result=$(node "$main_path/makeRepositoryPath.node.js" "$repositorys_info" 2>&1)
echo "$result"

cd $main_path
source "$main_path/init.sh"

cd $main_path
source "$main_path/pull.sh"

cd $main_path
source "$main_path/gitignore.sh"

# bash GRM.sh
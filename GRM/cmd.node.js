const { exec } = require("child_process")

module.exports = function (params = {}) {
  defaultValue = {
    命令: undefined,
    回调: function (result) { 
      if (!result[0]){
        console.log(result[1])
        process.exit(255)
      }
    },
    参数: {}
  }

  params = Object.assign(defaultValue, params)

  let undefined_keys = Object.keys(Object.fromEntries(Object.entries(params).filter(([key, value]) => [undefined].includes(value))))

  if (undefined_keys.length > 0) {
    console.log(undefined_keys.join(', ') + ', 参数值为空')
    return params.回调([false, undefined_keys.join(', ') + ', 参数值为空'])
  }

  exec(params.命令, (error, stdout, stderr) => {
    console.log('[' + (new Date().toLocaleTimeString()) + '] 开始执行: ' + params.命令)

    if (error) {
      return params.回调([false, error.message])
    }
    if (stderr) {
      return params.回调([false, stderr])
    }
    return params.回调([true, stdout])
  })

}

#!/bin/bash --login
main_path=$(cd $(dirname $0); pwd)

repositorys_info=$(node core.js)
jq '.' <<<$repositorys_info

repository_paths=($(jq -r 'keys_unsorted[]' <<<$repositorys_info))

for repository_path in "${repository_paths[@]}"; do
  repository_info=$(jq --arg key $repository_path '."\($key)"' <<<$repositorys_info)
  if [ ! -d "$repository_path" ]; then
    # echo "尚不存在该文件夹: $repository_path"
    # continue;
    mkdir -p $repository_path
    cd $repository_path
  else
    cd $repository_path
  fi

  if [ ! -d .git ]; then
    echo "开始初始化: $repository_path"
    git init
    git remote add origin "$(jq -r '."Git仓库地址"' <<<$repository_info)"
  else
    echo "已存在git信息: $repository_path"
  fi
done

# bash init.sh
class GitPlatform {
  constructor(params = {}) {
    let defaultValue = {
      Git用户名: undefined,
      Git仓库名: undefined,
      项目主页: undefined,
      Git仓库地址: undefined,
      远程文件路径: undefined,
      Git分支: 'master',
      Git提交方式: 'ssh',
      子仓库: [],
      Git忽略目录: [],
    }

    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

  }
}

class GitLab extends GitPlatform {
  constructor(params = {}) {
    super(params);
    params = Object.assign(this, params)

    switch (params.Git提交方式) {
      case 'ssh':
        if (!!!params.Git仓库地址) params.Git仓库地址 = 'git@gitlab.com:' + params.Git用户名 + '/' + params.Git仓库名 + '.git'
        break;
      default:
        params.Git提交方式 = undefined
        break;
    }

    if (!!!params.项目主页) params.项目主页 = 'https://gitlab.com/' + params.Git用户名 + '/' + params.Git仓库名
    if (!!!params.远程文件路径) params.远程文件路径 = 'https://gitlab.com/' + params.Git用户名 + '/' + params.Git仓库名 + '/-/raw/' + params.Git分支

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }
}

class Repository {
  constructor(params = {}) {
    let params_ = Object.entries(params)
    let local_path = params_[0][0]
    params = params_[0][1]

    let defaultValue = {
      说明: '暂无',
      本地路径: (params.本地路径 || local_path),
      // 本地路径: undefined,
      存储方式: 'GIT',
      远程平台: 'GitLab',
      主仓库: false,
    }

    params = Object.assign(defaultValue, params)

    switch (params.存储方式) {
      case 'GIT':
        break;
      default:
        params.存储方式 = undefined
        break;
    }

    switch (params.远程平台) {
      case 'GitLab':
        break;
      default:
        params.远程平台 = undefined
        break;
    }

    let undefined_keys = Object.keys(Object.fromEntries(Object.entries(this).filter(([key, value]) => [undefined].includes(value))))
    if (undefined_keys.length > 0) throw undefined_keys.join(', ') + ', 参数值为空';

    let attributes = eval('new ' + params.远程平台 + '(' + JSON.stringify(params) + ')')

    // 确保defaultValue键在前
    params = Object.assign(params, attributes)
    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

    for (let [key, value] of Object.entries(this.子仓库 || {})) {
      let rep = {}
      rep[key] = value
      let repository = new Repository(rep);
      this.addSubRepository(repository)
    }


  }

  // 添加子仓库
  addSubRepository(repository) {
    let undefined_keys = Object.keys(Object.fromEntries(Object.entries(repository).filter(([key, value]) => [undefined].includes(value))))
    if (undefined_keys.length > 0) {
      delete this.子仓库[repository.本地路径]
      return [false, undefined_keys.join(', ') + ', 参数值为空']
    }
    this.子仓库[repository.本地路径] = repository
    return [true, '']
  }

  // 解析当前对象
  analysisSelf(self, repository = self) {
    if (self != repository) {
      repository.本地路径 = self.本地路径 + '/' + repository.本地路径
    }

    Object.values(repository.子仓库 || {}).forEach(subRepository => {
      repository.analysisSelf(repository, subRepository)
      // repository.Git忽略目录.push(subRepository.本地路径.replace(new RegExp(repository.本地路径 + '/(.*)'), '$1/**'))
      // ubuntu 2020.04(arm64)上忽略文件夹后无需加**
      repository.Git忽略目录.push(subRepository.本地路径.replace(new RegExp(repository.本地路径 + '/(.*)'), '$1/'))
    })
  }
}

class Manger extends Repository {
  constructor(params = {}) {
    let params_ = Object.entries(params)
    let local_path = params_[0][0]
    params = params_[0][1]

    let rep = {}
    rep[local_path] = params
    let repository = super(rep);

    let undefined_keys = Object.keys(Object.fromEntries(Object.entries(this).filter(([key, value]) => [undefined].includes(value))))
    if (undefined_keys.length > 0) throw undefined_keys.join(', ') + ', 参数值为空';
  }

  structure() {
    if (!!this.仓库集合) return this.仓库集合
    this.analysisSelf(this)

    let analysisRep = (self, repository = self) => {
      if (self == repository) self.仓库集合 = {}

      self.仓库集合[repository.本地路径] = Object.fromEntries(
        Object.entries(repository).filter(
          ([key, value]) => !['子仓库', '仓库集合'].includes(key)
        )
      )

      Object.values(repository.子仓库 || {}).forEach(subRepository => {
        analysisRep(self, subRepository)
      })
    }

    analysisRep(this)

    return this.仓库集合
  }
}

let config = require('./register.node.js');

let manger = new Manger(config)

console.log(JSON.stringify(manger.structure(), null, 2))

module.exports = manger.structure();
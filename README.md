> 万物归一

<!-- TOC -->

- [说明](#说明)
  - [稀疏检出仓库](#稀疏检出仓库)
  - [关于文档标准格式及相关校验标准说明](#关于文档标准格式及相关校验标准说明)
    - [标点符号](#标点符号)
    - [换行标准](#换行标准)
    - [称谓标准](#称谓标准)
    - [叙述标准](#叙述标准)
    - [目录标准](#目录标准)
    - [空格标准](#空格标准)
    - [JSON格式标准](#json格式标准)
- [联系方式](#联系方式)

<!-- /TOC -->

# 说明

## 稀疏检出仓库

仓库中包含很多较大的音乐文件等, 可通过执行下面脚本进行稀疏检出,忽略这些文件:

```bash
curl -s -L https://gitlab.com/ff4c00/space/-/raw/master/space.sh | bash -s -- -m exclude_large_files
```

## 关于文档标准格式及相关校验标准说明

关于体系中Markdown文档应遵守以下规则:

### 标点符号

应采用英文标点符号,原因在于中文符号会占用更多的面积,<br>
曾有份76页的文档,只是将中文逗号和句号替换后文档页数就少了2页.

### 换行标准

原则上每行字数控制在25个左右,根据实际情况以逗号或句号进行换行.<br>
每行字数较少有利于读者阅读,部分的留白有助于阅读,没有人喜欢看密密麻麻的文字.<br>
根据行间阐述的内容进行分段,段落间留一个空行进行区分.

传统排版体系出于成本印刷等方面考虑,<br>
排版过于紧凑,整页密密麻麻的文字很容易引发焦虑,
该标准不考虑非必要因素,**一切以提升阅读体验为首要目标**.

### 称谓标准

文档内容应根据实际事实或理论知识进行阐述和解读,<br>
避免出现 *你*, *我*, *他*,等称谓,文字间不应带有任何感情色彩,仅限于原创内容,引用内容除外.

### 叙述标准

文档内容应该是成体系,结构严谨的,杜绝日记式流水账.

### 目录标准

目录应该是简洁易读的,目录结构所反映出来的是个人对于该知识体系的理解与掌握程度.

一个文档应该是对某门知识或者具体内容的深度探究.<br>
应该是成体系的,文档的结构是否合理,有时通过目录便可得知.<br>
结构漂亮的目录不一定合理,但是结构凌乱的目录说明文档所涉及的知识体系一定有问题.

目录应该代表的是一个知识体系,与该知识相关的内容应该能在目录中找到其位置.<br>
有些内容也许当前没有涉及,但是它是存在的,应当在体系内得以体现.

关于刚接触的领域,可以查找该领域较为权威的书籍,<br>
然后在京东上进行查找,京东的详情页内往往会附带html格式的书籍目录.<br>
稍微处理下即可成为该知识的目录.

知识体系结构并不是一成不变的,随着内容的扩充,理解的深入,目录也会随之产生多次重大的重构.

### 空格标准

中文间,中文与英文或数字间原则上不留有空格,<br>
该标准的一大特点是每行简短便于理解,<br>
无需添加空格帮助理解.

### JSON格式标准

```json
{"键": "值", "key": "value"}
```

0. 缩进以两个空格为准.
0. 键和冒号中间不存在空格.
0. 冒号和值中间存在一个空格.
0. 大括号和双引号之间不存在空格
0. 前后键之间,逗号跟随前值,并与后键间隔一个空格.

# 联系方式

个人邮箱: ff4c00@gmail.com